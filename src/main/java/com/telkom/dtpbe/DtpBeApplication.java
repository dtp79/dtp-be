package com.telkom.dtpbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DtpBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DtpBeApplication.class, args);
	}

}
