package com.telkom.dtpbe.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	//Get Product by Product Type ID
	@Query(value = "SELECT * FROM product WHERE product_type_id = :productTypeId", nativeQuery = true)
	List<Product> getProductByProductTypeID(@Param("productTypeId") Long productTypeId);
}
