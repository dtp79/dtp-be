package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ProductTypeDTO;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.Product;
import com.telkom.dtpbe.models.ProductType;
import com.telkom.dtpbe.repositories.ProductRepository;
import com.telkom.dtpbe.repositories.ProductTypeRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ProductTypeRepositoryController extends Minio {
ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ProductTypeRepository productTypeRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	public ProductTypeDTO convertToDTO(ProductType productType) {
		return modelMapper.map(productType, ProductTypeDTO.class);
	}

	public ProductType convertToEntity(ProductTypeDTO productTypeDTO) {
		return modelMapper.map(productTypeDTO, ProductType.class);
	}

	// Get All ProductType
	@SuppressWarnings("deprecation")
	@GetMapping("/productType/getAll")
	public HashMap<String, Object> getAllProductType() throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		List<HashMap<String, Object>> listProductTypes = new ArrayList<HashMap<String, Object>>();
		String imageUrl = "";
		String logoUrl = "";
		String message = "";
		
		for (ProductType productType : productTypeRepository.findAll()) {
			HashMap<String, Object> mapTemp = new HashMap<String, Object>();
			List<HashMap<String, Object>> listProduct = new ArrayList<HashMap<String, Object>>();
			
			ProductTypeDTO productTypeDTO = convertToDTO(productType);
						
			mapTemp.put("productTypeId",productTypeDTO.getProductTypeId());
			
			for(Product p :  productRepository.getProductByProductTypeID(productTypeDTO.getProductTypeId())) {
				HashMap<String, Object> mapProduct = new HashMap<String, Object>();
				
				
				mapProduct.put("productId", p.getProductId());
				mapProduct.put("productName", p.getProductName());
				mapProduct.put("description", p.getDescription());
				if (!p.getImagePath().isEmpty()) {
					imageUrl = minio().presignedGetObject(bucketName, p.getImagePath());
				} else {
					imageUrl = null;
				}
				mapProduct.put("imageUrl",imageUrl);
				
				listProduct.add(mapProduct);
				
			}
			
			mapTemp.put("product", listProduct);
			mapTemp.put("productTypeName",productTypeDTO.getProductTypeName());
			mapTemp.put("description",productTypeDTO.getDescription());
			if (!productType.getImagePath().isEmpty()) {
				imageUrl = minio().presignedGetObject(bucketName, productTypeDTO.getImagePath());
			} else {
				imageUrl = null;
			}
			mapTemp.put("imageUrl",imageUrl);
			
			if (!productType.getLogoPath().isEmpty()) {
				logoUrl = minio().presignedGetObject(bucketName, productTypeDTO.getLogoPath());
			} else {
				logoUrl = null;
			}
			mapTemp.put("logoUrl",logoUrl);
			
			listProductTypes.add(mapTemp);
		}
		
		if (listProductTypes.isEmpty()) {
			message = "Read All Failed!";
		} else {
			message = "Read All Success!";
		}
		mapResult.put("message", message);
		mapResult.put("total", listProductTypes.size());
		mapResult.put("data", listProductTypes);

		return mapResult;
	}
}
