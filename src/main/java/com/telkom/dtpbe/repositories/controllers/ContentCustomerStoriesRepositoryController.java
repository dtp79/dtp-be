package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentCustomerStoriesDTO;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.ContentCustomerStories;
import com.telkom.dtpbe.repositories.ContentCustomerStoriesRepository;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentCustomerStoriesRepositoryController extends Minio{
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentCustomerStoriesRepository contentCustomerStoriesRepository;
	
	public ContentCustomerStoriesDTO convertToDTO(ContentCustomerStories contentCustomerStories) {
		return modelMapper.map(contentCustomerStories, ContentCustomerStoriesDTO.class);
	}

	public ContentCustomerStories convertToEntity(ContentCustomerStoriesDTO contentCustomerStoriesDTO) {
		return modelMapper.map(contentCustomerStoriesDTO, ContentCustomerStories.class);
	}

	// Get All ContentCustomerStories
	@SuppressWarnings("deprecation")
	@GetMapping("/contentCustomerStories/getAll")
	public HashMap<String, Object> getAllContentCustomerStories() throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<HashMap<String, Object>> listContentCustomerStoriess = new ArrayList<HashMap<String, Object>>();
		
		String imageUrl = null;
		String logoUrl = null;
		String message = null;
		
		for (ContentCustomerStories c : contentCustomerStoriesRepository.findAll()) {
			ContentCustomerStoriesDTO contentCustomerStoriesDTO = convertToDTO(c);
			
			HashMap<String, Object> mapTemp = new HashMap<String, Object>();
			mapTemp.put("customerStoriesId",contentCustomerStoriesDTO.getCustomerStoriesId());
			mapTemp.put("users",contentCustomerStoriesDTO.getUsers());
			mapTemp.put("title",contentCustomerStoriesDTO.getTitle());
			mapTemp.put("longStories",contentCustomerStoriesDTO.getLongStories());
			mapTemp.put("shortStories",contentCustomerStoriesDTO.getShortStories());
			mapTemp.put("imagePath",contentCustomerStoriesDTO.getImagePath());
			mapTemp.put("description",contentCustomerStoriesDTO.getDescription());
			if (c.getImagePath() != null) {
				imageUrl = minio().presignedGetObject(bucketName, contentCustomerStoriesDTO.getImagePath());
			}
			mapTemp.put("imageUrl",imageUrl);
			if (c.getUsers().getImagePath() != null) {
				logoUrl = minio().presignedGetObject(bucketName, contentCustomerStoriesDTO.getUsers().getImagePath());
			}
			mapTemp.put("logoUrl",logoUrl);
			
			listContentCustomerStoriess.add(mapTemp);
		}

		if (listContentCustomerStoriess.isEmpty()) {
			message = "Read All Failed!";
		} else {
			message = "Read All Success!";
		}
		showHashMap.put("message", message);
		showHashMap.put("total", listContentCustomerStoriess.size());
		showHashMap.put("data", listContentCustomerStoriess);

		return showHashMap;
	}
}
