package com.telkom.dtpbe.repositories.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telkom.dtpbe.dtos.ContentAdvantageDtpDTO;
import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.ContentAdvantageDtp;
import com.telkom.dtpbe.repositories.ContentAdvantageDtpRepository;

import io.minio.PutObjectOptions;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import lombok.Data;
import lombok.NoArgsConstructor;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentAdvantageDtpRepositoryController extends Minio{
ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentAdvantageDtpRepository contentAdvantageDtpRepository;
	
	public ContentAdvantageDtpDTO convertToDTO(ContentAdvantageDtp contentAdvantageDtp) {
		return modelMapper.map(contentAdvantageDtp, ContentAdvantageDtpDTO.class);
	}

	public ContentAdvantageDtp convertToEntity(ContentAdvantageDtpDTO contentAdvantageDtpDTO) {
		return modelMapper.map(contentAdvantageDtpDTO, ContentAdvantageDtp.class);
	}
	
	// Get All ContentAdvantageDtp
	@SuppressWarnings("deprecation")
	@GetMapping("/contentAdvantageDtp/getAll")
	public HashMap<String, Object> getAllContentAdvantageDtp() throws InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, ServerException, XmlParserException, InvalidEndpointException, InvalidPortException, IOException {
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		
		List<HashMap<String, Object>> listContentAdvantageDtps = new ArrayList<HashMap<String, Object>>();
		
		String imageUrl = null;
		String message = null;
		
		for (ContentAdvantageDtp c : contentAdvantageDtpRepository.findAll()) {
			ContentAdvantageDtpDTO contentAdvantageDtpDTO = convertToDTO(c);
			
			HashMap<String, Object> mapTemp = new HashMap<String, Object>();
			mapTemp.put("advantageDtpId",contentAdvantageDtpDTO.getAdvantageDtpId());
			mapTemp.put("title",contentAdvantageDtpDTO.getTitle());
			mapTemp.put("advantage",contentAdvantageDtpDTO.getAdvantage());
			mapTemp.put("imagePath",contentAdvantageDtpDTO.getImagePath());
			mapTemp.put("description",contentAdvantageDtpDTO.getDescription());
			if (c.getImagePath() != null) {
				imageUrl = minio().presignedGetObject(bucketName, contentAdvantageDtpDTO.getImagePath());
			}
			mapTemp.put("imageUrl",imageUrl);
			
			listContentAdvantageDtps.add(mapTemp);
		}
		
		if (listContentAdvantageDtps.isEmpty()) {
			message = "Read All Failed!";
		} else {
			message = "Read All Success!";
		}
		mapResult.put("message", message);
		mapResult.put("total", listContentAdvantageDtps.size());
		mapResult.put("data", listContentAdvantageDtps);

		return mapResult;
	}
	
	// Create a new ContentAdvantageDtp
	@SuppressWarnings({ "deprecation", "unused" })
	@PostMapping("/contentAdvantageDtp/create")
	public HashMap<String, Object> createContentAdvantageDtp(
			@Valid @RequestBody AdvantageDtpLanguage advantageDtpLanguage, 
			@RequestParam(value = "file") MultipartFile file, 
			@RequestParam(value = "textObject") String textObject) throws JsonParseException, JsonMappingException, IOException, InvalidKeyException, ErrorResponseException,
			IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException,
			InvalidResponseException, NoSuchAlgorithmException, XmlParserException, InvalidEndpointException,
			InvalidPortException, ServerException {
		
		HashMap<String, Object> mapResult = new HashMap<String, Object>();
		HashMap<String, Object> mapTitle = new HashMap<String, Object>();
		HashMap<String, Object> mapAdvantage = new HashMap<String, Object>();
		List<HashMap<String, Object>> listTitleLanguage = new ArrayList<HashMap<String, Object>>();
		List<HashMap<String, Object>> listAdvantageLanguage = new ArrayList<HashMap<String, Object>>();
		LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		String message = null;
		String imagePath = null;
		Long id = new Long(1);
		
		mapTitle.put(advantageDtpLanguage.getLanguageId(), advantageDtpLanguage.getTitle());
		mapAdvantage.put(advantageDtpLanguage.getLanguageId(), advantageDtpLanguage.getAdvantage());
		listTitleLanguage.add(mapTitle);
		listAdvantageLanguage.add(mapAdvantage);
		
		ContentAdvantageDtp contentAdvantageDtp = new ContentAdvantageDtp();
		contentAdvantageDtp.setAdvantageDtpId(advantageDtpLanguage.getAdvantageDtpId());
		contentAdvantageDtp.setTitle(listTitleLanguage.toString());
		contentAdvantageDtp.setAdvantage(listAdvantageLanguage.toString());
		if (!file.isEmpty()) {
			imagePath = "content/" + file.getOriginalFilename();
			Path filePath = Files.createTempFile("file", file.getContentType());
			file.transferTo(filePath);
			PutObjectOptions options = new PutObjectOptions(file.getSize(), -1);
			minio().putObject(bucketName, imagePath, filePath.toString(), options);
			Files.delete(filePath);
		}
		contentAdvantageDtp.setImagePath(imagePath);
		contentAdvantageDtp.setDescription(advantageDtpLanguage.getDescription());
		contentAdvantageDtp.setCreatedBy(id);
		contentAdvantageDtp.setCreatedOn(dateNow);
		contentAdvantageDtp.setLastModifiedBy(id);
		contentAdvantageDtp.setLastModifiedOn(dateNow);
    		
    	contentAdvantageDtpRepository.save(contentAdvantageDtp);
    	
    	if(contentAdvantageDtp == null) {
    		message = "Create Failed!";
    	} else {
    		message = "Create Success!";
    	}
    	
    	mapResult.put("message", message);
    	mapResult.put("data", contentAdvantageDtp);
    	
    	return mapResult;
    }
	
	@Data
	@NoArgsConstructor
	public class AdvantageDtpLanguage {
		private long advantageDtpId;
		private String languageId;
		private String title;
		private String advantage;
		private String description;
	}
}
