package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.ContentAdvantageDtp;

@Repository
public interface ContentAdvantageDtpRepository extends JpaRepository<ContentAdvantageDtp, Long>{

}
