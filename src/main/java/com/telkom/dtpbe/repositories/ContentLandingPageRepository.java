package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.ContentLandingPage;

@Repository
public interface ContentLandingPageRepository extends JpaRepository<ContentLandingPage, Long> {

	
	
}
