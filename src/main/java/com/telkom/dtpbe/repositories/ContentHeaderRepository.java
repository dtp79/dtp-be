package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.ContentHeader;

@Repository
public interface ContentHeaderRepository extends JpaRepository<ContentHeader, Long> {

	
	
}
