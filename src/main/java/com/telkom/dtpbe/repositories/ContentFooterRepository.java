package com.telkom.dtpbe.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telkom.dtpbe.models.ContentFooter;

@Repository
public interface ContentFooterRepository extends JpaRepository<ContentFooter, Long> {

	
	
}
