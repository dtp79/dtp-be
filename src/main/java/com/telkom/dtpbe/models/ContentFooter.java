package com.telkom.dtpbe.models;
// Generated Aug 18, 2020 5:29:44 PM by Hibernate Tools 4.3.5.Final

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "content_footer", schema = "public")
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class ContentFooter implements java.io.Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_content_footer_id_seq")
	@SequenceGenerator(name = "generator_content_footer_id_seq", sequenceName = "content_footer_id_seq", schema = "public", allocationSize = 1)
	@Column(name = "content_footer_id", unique = true, nullable = false)
	private Long contentFooterId;
	
	@Column(name = "title")
	private String title;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "is_deleted")
	private Short isDeleted;
	
	@Column(name = "created_by")
	@CreatedBy
	private Long createdBy;
	
	@Column(name = "created_on", length = 13, updatable = false)
	@CreationTimestamp
	private Timestamp createdOn;
	
	@Column(name = "last_modified_by")
	@LastModifiedBy
	private Long lastModifiedBy;
	
	@Column(name = "last_modified_on", length = 13)
	@UpdateTimestamp	
	private Timestamp lastModifiedOn;

}
