package com.telkom.dtpbe.models;
// Generated Aug 26, 2020 2:09:09 PM by Hibernate Tools 4.3.5.Final

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ContentAdvantageDtp generated by hbm2java
 */
@Entity
@Table(name = "content_advantage_dtp", schema = "public")
public class ContentAdvantageDtp implements java.io.Serializable {

	private long advantageDtpId;
	private String title;
	private String advantage;
	private String imagePath;
	private String description;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;

	public ContentAdvantageDtp() {
	}

	public ContentAdvantageDtp(long advantageDtpId) {
		this.advantageDtpId = advantageDtpId;
	}

	public ContentAdvantageDtp(long advantageDtpId, String title, String advantage, String imagePath,
			String description, Boolean isDeleted, Long createdBy, Timestamp createdOn, Long lastModifiedBy,
			Timestamp lastModifiedOn) {
		this.advantageDtpId = advantageDtpId;
		this.title = title;
		this.advantage = advantage;
		this.imagePath = imagePath;
		this.description = description;
		this.isDeleted = isDeleted;
		this.createdBy = createdBy;
		this.createdOn = createdOn;
		this.lastModifiedBy = lastModifiedBy;
		this.lastModifiedOn = lastModifiedOn;
	}

	@Id

	@Column(name = "advantage_dtp_id", unique = true, nullable = false)
	public long getAdvantageDtpId() {
		return this.advantageDtpId;
	}

	public void setAdvantageDtpId(long advantageDtpId) {
		this.advantageDtpId = advantageDtpId;
	}

	@Column(name = "title")
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "advantage")
	public String getAdvantage() {
		return this.advantage;
	}

	public void setAdvantage(String advantage) {
		this.advantage = advantage;
	}

	@Column(name = "image_path")
	public String getImagePath() {
		return this.imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "is_deleted")
	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Column(name = "created_by")
	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "created_on", length = 29)
	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	@Column(name = "last_modified_by")
	public Long getLastModifiedBy() {
		return this.lastModifiedBy;
	}

	public void setLastModifiedBy(Long lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	@Column(name = "last_modified_on", length = 29)
	public Timestamp getLastModifiedOn() {
		return this.lastModifiedOn;
	}

	public void setLastModifiedOn(Timestamp lastModifiedOn) {
		this.lastModifiedOn = lastModifiedOn;
	}

}
