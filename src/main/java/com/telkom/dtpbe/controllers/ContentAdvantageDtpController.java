package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentAdvantageDtpDTO;
import com.telkom.dtpbe.models.ContentAdvantageDtp;
import com.telkom.dtpbe.repositories.ContentAdvantageDtpRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentAdvantageDtpController {
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentAdvantageDtpRepository contentAdvantageDtpRepository;
	
	public ContentAdvantageDtpDTO convertToDTO(ContentAdvantageDtp contentAdvantageDtp) {
		return modelMapper.map(contentAdvantageDtp, ContentAdvantageDtpDTO.class);
	}

	public ContentAdvantageDtp convertToEntity(ContentAdvantageDtpDTO contentAdvantageDtpDTO) {
		return modelMapper.map(contentAdvantageDtpDTO, ContentAdvantageDtp.class);
	}

	// Get All ContentAdvantageDtp
	@GetMapping("/contentAdvantageDtp/readAll")
	public HashMap<String, Object> getAllContentAdvantageDtp() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<ContentAdvantageDtpDTO> listContentAdvantageDtps = new ArrayList<ContentAdvantageDtpDTO>();
		for (ContentAdvantageDtp c : contentAdvantageDtpRepository.findAll()) {
			ContentAdvantageDtpDTO contentAdvantageDtpDTO = convertToDTO(c);
			listContentAdvantageDtps.add(contentAdvantageDtpDTO);
		}

		String message;
		if (listContentAdvantageDtps.isEmpty()) {
			message = "Read All Failed!";
		} else {
			message = "Read All Success!";
		}
		showHashMap.put("message", message);
		showHashMap.put("total", listContentAdvantageDtps.size());
		showHashMap.put("data", listContentAdvantageDtps);

		return showHashMap;
	}

	// Read ContentAdvantageDtp By ID
	@GetMapping("/contentAdvantageDtp/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		ContentAdvantageDtp contentAdvantageDtp = contentAdvantageDtpRepository.findById(id).orElse(null);
		ContentAdvantageDtpDTO contentAdvantageDtpDTO = convertToDTO(contentAdvantageDtp);
		showHashMap.put("messages", "Read data Success");
		showHashMap.put("data", contentAdvantageDtpDTO);
		return showHashMap;
	}

	// Create a new ContentAdvantageDtp
	@PostMapping("/contentAdvantageDtp/add")
	public HashMap<String, Object> createContentAdvantageDtp(@Valid @RequestBody ContentAdvantageDtpDTO contentAdvantageDtpDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	Long id = new Long(1);
    	String message;
    	LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			ContentAdvantageDtp contentAdvantageDtp = convertToEntity(contentAdvantageDtpDTO);
			contentAdvantageDtp.setIsDeleted(false);
			contentAdvantageDtp.setCreatedBy(id);
			contentAdvantageDtp.setCreatedOn(dateNow);
			contentAdvantageDtp.setLastModifiedBy(id);
			contentAdvantageDtp.setLastModifiedOn(dateNow);
			contentAdvantageDtpRepository.save(contentAdvantageDtp);
			message = "Create Success!";
			showHashMap.put("data", contentAdvantageDtp);
		} catch (Exception e) {
			message = "Create Failed!" + e.getMessage();
		}
		
    	showHashMap.put("message", message);
    	
    	
    	return showHashMap;
    }

	// Update a ContentAdvantageDtp
	@PutMapping("/contentAdvantageDtp/update/{id}")
	public HashMap<String, Object> updateContentAdvantageDtp(@PathVariable(value = "id") Long id,
			@Valid @RequestBody ContentAdvantageDtpDTO contentAdvantageDtpDetails) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;

		ContentAdvantageDtp contentAdvantageDtp = contentAdvantageDtpRepository.findById(id).orElse(null);
		
		contentAdvantageDtpDetails.setAdvantageDtpId(contentAdvantageDtp.getAdvantageDtpId());

		if (contentAdvantageDtpDetails.getTitle() != null) {
			contentAdvantageDtp.setTitle(convertToEntity(contentAdvantageDtpDetails).getTitle());
		}
		if (contentAdvantageDtpDetails.getAdvantage() != null) {
			contentAdvantageDtp.setAdvantage(convertToEntity(contentAdvantageDtpDetails).getAdvantage());
		}
		if (contentAdvantageDtpDetails.getImagePath() != null) {
			contentAdvantageDtp.setImagePath(convertToEntity(contentAdvantageDtpDetails).getImagePath());
		}
		if (contentAdvantageDtpDetails.getDescription() != null) {
			contentAdvantageDtp.setDescription(convertToEntity(contentAdvantageDtpDetails).getDescription());
		}
		if (contentAdvantageDtpDetails.getIsDeleted() != null) {
			contentAdvantageDtp.setIsDeleted(convertToEntity(contentAdvantageDtpDetails).getIsDeleted());
		}
		if (contentAdvantageDtpDetails.getCreatedBy() != null) {
			contentAdvantageDtp.setCreatedBy(convertToEntity(contentAdvantageDtpDetails).getCreatedBy());
		}
		if (contentAdvantageDtpDetails.getCreatedOn() != null) {
			contentAdvantageDtp.setCreatedOn(convertToEntity(contentAdvantageDtpDetails).getCreatedOn());
		}
		if (contentAdvantageDtpDetails.getLastModifiedBy() != null) {
			contentAdvantageDtp.setLastModifiedBy(convertToEntity(contentAdvantageDtpDetails).getLastModifiedBy());
		}
		if (contentAdvantageDtpDetails.getLastModifiedOn() != null) {
			contentAdvantageDtp.setLastModifiedOn(convertToEntity(contentAdvantageDtpDetails).getLastModifiedOn());
		}
		ContentAdvantageDtp updateContentAdvantageDtp = contentAdvantageDtpRepository.save(contentAdvantageDtp);

		List<ContentAdvantageDtp> resultList = new ArrayList<ContentAdvantageDtp>();
		resultList.add(updateContentAdvantageDtp);

		if (resultList.isEmpty()) {
			message = "Update Failed!";
		} else {
			message = "Update Success!";
		}

		showHashMap.put("message", message);
		showHashMap.put("total", resultList.size());
		showHashMap.put("data", resultList);

		return showHashMap;
	}

	// Delete a ContentAdvantageDtp
	@DeleteMapping("/contentAdvantageDtp/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		ContentAdvantageDtp contentAdvantageDtp = contentAdvantageDtpRepository.findById(id).orElse(null);

		ContentAdvantageDtpDTO contentAdvantageDtpDTO = convertToDTO(contentAdvantageDtp);
		contentAdvantageDtpRepository.delete(contentAdvantageDtp);

		showHashMap.put("messages", "Delete data Success!");
		showHashMap.put("deleteData :", contentAdvantageDtpDTO);
		return showHashMap;
	}
}
