package com.telkom.dtpbe.controllers.customAPI;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.interfaces.Minio;
import com.telkom.dtpbe.models.ContentLandingPage;
import com.telkom.dtpbe.repositories.ContentLandingPageRepository;

import io.minio.MinioClient;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidExpiresRangeException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;


@RestController
@RequestMapping("landingPage")
public class ShowAndSetLandingPage extends Minio{

	@Autowired
	ContentLandingPageRepository contentLandingPageRepository;

	// Show All LandingPage
	@GetMapping("/carousel")
	public HashMap<String, Object> showCarousel() throws InvalidEndpointException, InvalidPortException, InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidExpiresRangeException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException, ServerException{
		HashMap<String,Object> result = new HashMap<String,Object>();
		HashMap<String,Object> dataHashMap = new HashMap<String,Object>();
		ArrayList<HashMap<String, Object>> listData = new ArrayList<HashMap<String, Object>>();
		MinioClient minioClient = minio();
		
		for(ContentLandingPage data : contentLandingPageRepository.findAll()) {
			String image = minioClient.presignedGetObject(bucketName, data.getImagePath());
			dataHashMap.put("title", data.getDescription());
			
			if(data.getImagePath() == null) {
				dataHashMap.put("image", null);
			}else {
				dataHashMap.put("image", image);
			}
			
			listData.add(dataHashMap);
			dataHashMap = new HashMap<String,Object>();
		}
		
		String message;
		String status;
        if(listData.isEmpty()) {
    		message = "Read All Failed!";
    		status = HttpStatus.NOT_FOUND.toString();
    	} else {
    		message = "Read All Success!";
    		status = HttpStatus.OK.toString();
    		result.put("Data", listData);
    	}
        
        result.put("Status", status);
		result.put("Message", message);
		return result;
	}
	
	
}
