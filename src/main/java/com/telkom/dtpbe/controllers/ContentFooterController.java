package com.telkom.dtpbe.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentFooterDTO;
import com.telkom.dtpbe.models.ContentFooter;
import com.telkom.dtpbe.repositories.ContentFooterRepository;


@RestController
@RequestMapping("footer")
public class ContentFooterController{

	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentFooterRepository contentFooterRepository;

	public ContentFooterDTO convertToDTO(ContentFooter object) {
	        return modelMapper.map(object, ContentFooterDTO.class);
	}

	public ContentFooter convertToEntity(ContentFooterDTO object) {
	    return modelMapper.map(object, ContentFooter.class);
	}
	
	//View All Data
	@GetMapping("/all")
	public HashMap<String, Object> getAllData() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		ArrayList<ContentFooterDTO> listData = new ArrayList<ContentFooterDTO>();
		
		for(ContentFooter data : contentFooterRepository.findAll()) {
			ContentFooterDTO convertedData = convertToDTO(data);
			listData.add(convertedData);
		}
		
		String message;
		String status;
        if(listData.isEmpty()) {
    		message = "Read All Failed!";
    		status = HttpStatus.NOT_FOUND.toString();
    	} else {
    		message = "Read All Success!";
    		status = HttpStatus.OK.toString();
    	}
        
        result.put("Status", status);
		result.put("Message", message);
		result.put("Data", listData);
		
		return result;
	}
	
	//View Data By ID
	@GetMapping("/byId/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ContentFooter data = contentFooterRepository.findById(id).orElse(null);
		ContentFooterDTO convertedData = convertToDTO(data);
		
		String message;
		String status;
		
        if(convertedData == null) {
    		message = "Read Data Failed!";
    		status = HttpStatus.NOT_FOUND.toString();
    	} else {
    		message = "Read Data Success!";
    		status = HttpStatus.OK.toString();
    	}
        
        result.put("Status", status);
		result.put("Message", message);
		result.put("Data", convertedData);
		return result;
	}
	
	// Create a new data
	@PostMapping("/add")
	public HashMap<String, Object> createData(@Valid @RequestBody ArrayList<ContentFooterDTO> dataDto) {
    	HashMap<String, Object> result = new HashMap<String, Object>();
    	@Valid ArrayList<ContentFooterDTO> listData = dataDto;
    	String message;
		String status;
		
        if(dataDto.isEmpty()) {
    		message = "Add Data Failed!";
    		status = HttpStatus.UNPROCESSABLE_ENTITY.toString();
    	} else {
    		for(ContentFooterDTO temporaryData : listData) {
    			ContentFooter data = convertToEntity(temporaryData);
    			contentFooterRepository.save(data);
        	}
    		message = "Add Data Success!";
    		status = HttpStatus.CREATED.toString();
    	}
 
        result.put("Status", status);
    	result.put("Message", message);
    	result.put("Total Insert", listData.size());
    	result.put("Data", listData);
    	return result;
    }
	
	// UPDATE BELUM
	@PutMapping("/update/{id}")
	public HashMap<String, Object> updateData(@PathVariable(value="id") Long id, @Valid @RequestBody ContentFooterDTO dataDto){
		HashMap<String, Object> result = new HashMap<String, Object>();		
		String message;
		String status;
		
		ContentFooter contentLandingPage = contentFooterRepository.findById(id).orElseThrow(null);
		
        if(contentLandingPage == null) {
    		message = "Update Failed!";
    		status = HttpStatus.BAD_REQUEST.toString();
    	} else {
    		dataDto.setContentFooterId(contentLandingPage.getContentFooterId());
    		
    		if(dataDto.getTitle() != null) {
    			contentLandingPage.setTitle(convertToEntity(dataDto).getTitle());
    		}
    		
    		if(dataDto.getDescription() != null) {
    			contentLandingPage.setDescription(convertToEntity(dataDto).getDescription());
    		}
    		
    		if(dataDto.getIsDeleted() != null) {
    			contentLandingPage.setIsDeleted(convertToEntity(dataDto).getIsDeleted());
    		}
    		
    		if(dataDto.getCreatedBy() != null){
    			contentLandingPage.setCreatedBy(convertToEntity(dataDto).getCreatedBy());
    		}
    		
    		if(dataDto.getCreatedOn() != null) {
    			contentLandingPage.setCreatedOn(convertToEntity(dataDto).getCreatedOn());
    		}
    		
    		if(dataDto.getLastModifiedBy() != null) {
    			contentLandingPage.setLastModifiedBy(convertToEntity(dataDto).getLastModifiedBy());
    		}
    		
    		if(dataDto.getLastModifiedOn() != null) {
    			contentLandingPage.setLastModifiedOn(convertToEntity(dataDto).getLastModifiedOn());
    		}
    		message = "Update Success!";
    		status = HttpStatus.OK.toString();
    		
    		contentFooterRepository.save(contentLandingPage);
    	}
		
		result.put("Status", status);
		result.put("Message : ", message);
		result.put("Data : ", contentLandingPage);
		
		return result;
	}
	
	// Delete a Data
    @DeleteMapping("/delete/{id}")
    public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
    	HashMap<String, Object> result = new HashMap<String, Object>();
    	ContentFooter data = contentFooterRepository.findById(id)
    			.orElse(null);
    	ContentFooterDTO dataDTO = null;
    	String message;
		String status;
    	
    	if(data == null) {
    		message = "Delete Data Failed!";
    		status = HttpStatus.NOT_FOUND.toString();
    	}else {
    		dataDTO = convertToDTO(data);
    		message = "Delete Data Success!";
    		status = HttpStatus.OK.toString();
    		contentFooterRepository.delete(data);
    	}
    	
    	result.put("Status", status);
        result.put("Messages", message);
        result.put("Delete data :", dataDTO);
    	return result;
    }
}
