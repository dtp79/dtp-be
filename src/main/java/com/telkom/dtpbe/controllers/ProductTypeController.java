package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ProductTypeDTO;
import com.telkom.dtpbe.models.ProductType;
import com.telkom.dtpbe.repositories.ProductTypeRepository;

@RestController
@RequestMapping("/api")
public class ProductTypeController {
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ProductTypeRepository productTypeRepository;
	
	public ProductTypeDTO convertToDTO(ProductType productType) {
		return modelMapper.map(productType, ProductTypeDTO.class);
	}

	public ProductType convertToEntity(ProductTypeDTO productTypeDTO) {
		return modelMapper.map(productTypeDTO, ProductType.class);
	}

	// Get All ProductType
	@GetMapping("/productType/readAll")
	public HashMap<String, Object> getAllProductType() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<ProductTypeDTO> listProductTypes = new ArrayList<ProductTypeDTO>();
		for (ProductType p : productTypeRepository.findAll()) {
			ProductTypeDTO productTypeDTO = convertToDTO(p);
			listProductTypes.add(productTypeDTO);
		}

		String message;
		if (listProductTypes.isEmpty()) {
			message = "Read All Failed!";
		} else {
			message = "Read All Success!";
		}
		showHashMap.put("message", message);
		showHashMap.put("total", listProductTypes.size());
		showHashMap.put("data", listProductTypes);

		return showHashMap;
	}

	// Read ProductType By ID
	@GetMapping("/productType/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		ProductType productType = productTypeRepository.findById(id).orElse(null);
		ProductTypeDTO productTypeDTO = convertToDTO(productType);
		showHashMap.put("messages", "Read data Success");
		showHashMap.put("data", productTypeDTO);
		return showHashMap;
	}

	// Create a new ProductType
	@PostMapping("/productType/add")
	public HashMap<String, Object> createProductType(@Valid @RequestBody ProductTypeDTO productTypeDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	Long id = new Long(1);
    	String message;
    	LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			ProductType productType = convertToEntity(productTypeDTO);
			productType.setIsDeleted(false);
			productType.setCreatedBy(id);
			productType.setCreatedOn(dateNow);
			productType.setLastModifiedBy(id);
			productType.setLastModifiedOn(dateNow);
			productTypeRepository.save(productType);
			message = "Create Success!";
			showHashMap.put("data", productType);
		} catch (Exception e) {
			message = "Create Failed!" + e.getMessage();
		}
		
    	showHashMap.put("message", message);
    	
    	return showHashMap;
    }

	// Update a ProductType
	@PutMapping("/productType/update/{id}")
	public HashMap<String, Object> updateProductType(@PathVariable(value = "id") Long id,
			@Valid @RequestBody ProductTypeDTO productTypeDetails) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;

		ProductType productType = productTypeRepository.findById(id).orElse(null);
		
		productTypeDetails.setProductTypeId(productType.getProductTypeId());

		if (productTypeDetails.getProductTypeName() != null) {
			productType.setProductTypeName(convertToEntity(productTypeDetails).getProductTypeName());
		}
		if (productTypeDetails.getImagePath() != null) {
			productType.setImagePath(convertToEntity(productTypeDetails).getImagePath());
		}
		if (productTypeDetails.getDescription() != null) {
			productType.setDescription(convertToEntity(productTypeDetails).getDescription());
		}
		if (productTypeDetails.getIsDeleted() != null) {
			productType.setIsDeleted(convertToEntity(productTypeDetails).getIsDeleted());
		}
		if (productTypeDetails.getCreatedBy() != null) {
			productType.setCreatedBy(convertToEntity(productTypeDetails).getCreatedBy());
		}
		if (productTypeDetails.getCreatedOn() != null) {
			productType.setCreatedOn(convertToEntity(productTypeDetails).getCreatedOn());
		}
		if (productTypeDetails.getLastModifiedBy() != null) {
			productType.setLastModifiedBy(convertToEntity(productTypeDetails).getLastModifiedBy());
		}
		if (productTypeDetails.getLastModifiedOn() != null) {
			productType.setLastModifiedOn(convertToEntity(productTypeDetails).getLastModifiedOn());
		}
		ProductType updateProductType = productTypeRepository.save(productType);

		List<ProductType> resultList = new ArrayList<ProductType>();
		resultList.add(updateProductType);

		if (resultList.isEmpty()) {
			message = "Update Failed!";
		} else {
			message = "Update Success!";
		}

		showHashMap.put("message", message);
		showHashMap.put("total", resultList.size());
		showHashMap.put("data", resultList);

		return showHashMap;
	}

	// Delete a ProductType
	@DeleteMapping("/productType/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		ProductType productType = productTypeRepository.findById(id).orElse(null);

		ProductTypeDTO productTypeDTO = convertToDTO(productType);
		productTypeRepository.delete(productType);

		showHashMap.put("messages", "Delete data Success!");
		showHashMap.put("deleteData :", productTypeDTO);
		return showHashMap;
	}
}
