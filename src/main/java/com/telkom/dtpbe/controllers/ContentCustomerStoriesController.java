package com.telkom.dtpbe.controllers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telkom.dtpbe.dtos.ContentCustomerStoriesDTO;
import com.telkom.dtpbe.models.ContentCustomerStories;
import com.telkom.dtpbe.repositories.ContentCustomerStoriesRepository;

@CrossOrigin(allowCredentials="true")
@RestController
@RequestMapping("/api")
public class ContentCustomerStoriesController {
	
	ModelMapper modelMapper = new ModelMapper();
	
	@Autowired
	ContentCustomerStoriesRepository contentCustomerStoriesRepository;
	
	public ContentCustomerStoriesDTO convertToDTO(ContentCustomerStories contentCustomerStories) {
		return modelMapper.map(contentCustomerStories, ContentCustomerStoriesDTO.class);
	}

	public ContentCustomerStories convertToEntity(ContentCustomerStoriesDTO contentCustomerStoriesDTO) {
		return modelMapper.map(contentCustomerStoriesDTO, ContentCustomerStories.class);
	}

	// Get All ContentCustomerStories
	@GetMapping("/contentCustomerStories/readAll")
	public HashMap<String, Object> getAllContentCustomerStories() {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		List<ContentCustomerStoriesDTO> listContentCustomerStoriess = new ArrayList<ContentCustomerStoriesDTO>();
		for (ContentCustomerStories c : contentCustomerStoriesRepository.findAll()) {
			ContentCustomerStoriesDTO contentCustomerStoriesDTO = convertToDTO(c);
			listContentCustomerStoriess.add(contentCustomerStoriesDTO);
		}

		String message;
		if (listContentCustomerStoriess.isEmpty()) {
			message = "Read All Failed!";
		} else {
			message = "Read All Success!";
		}
		showHashMap.put("message", message);
		showHashMap.put("total", listContentCustomerStoriess.size());
		showHashMap.put("data", listContentCustomerStoriess);

		return showHashMap;
	}

	// Read ContentCustomerStories By ID
	@GetMapping("/contentCustomerStories/read/{id}")
	public HashMap<String, Object> getById(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		ContentCustomerStories contentCustomerStories = contentCustomerStoriesRepository.findById(id).orElse(null);
		ContentCustomerStoriesDTO contentCustomerStoriesDTO = convertToDTO(contentCustomerStories);
		showHashMap.put("messages", "Read data Success");
		showHashMap.put("data", contentCustomerStoriesDTO);
		return showHashMap;
	}

	// Create a new ContentCustomerStories
	@PostMapping("/contentCustomerStories/add")
	public HashMap<String, Object> createContentCustomerStories(@Valid @RequestBody ContentCustomerStoriesDTO contentCustomerStoriesDTO) {
    	HashMap<String, Object> showHashMap = new HashMap<String, Object>();
    	Long id = new Long(1);
    	String message;
    	LocalDateTime localNow = LocalDateTime.now();
		Timestamp dateNow = Timestamp.valueOf(localNow);
		
		try {
			ContentCustomerStories contentCustomerStories = convertToEntity(contentCustomerStoriesDTO);
			contentCustomerStories.setIsDeleted(false);
			contentCustomerStories.setCreatedBy(id);
			contentCustomerStories.setCreatedOn(dateNow);
			contentCustomerStories.setLastModifiedBy(id);
			contentCustomerStories.setLastModifiedOn(dateNow);
			contentCustomerStoriesRepository.save(contentCustomerStories);
			message = "Create Success!";
			showHashMap.put("data", contentCustomerStories);
		} catch (Exception e) {
			message = "Create Failed!" + e.getMessage();
		}
		
    	showHashMap.put("message", message);
    	
    	return showHashMap;
    }

	// Update a ContentCustomerStories
	@PutMapping("/contentCustomerStories/update/{id}")
	public HashMap<String, Object> updateContentCustomerStories(@PathVariable(value = "id") Long id,
			@Valid @RequestBody ContentCustomerStoriesDTO contentCustomerStoriesDetails) {

		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		String message;

		ContentCustomerStories contentCustomerStories = contentCustomerStoriesRepository.findById(id).orElse(null);
		
		contentCustomerStoriesDetails.setCustomerStoriesId(contentCustomerStories.getCustomerStoriesId());

		if (contentCustomerStoriesDetails.getTitle() != null) {
			contentCustomerStories.setTitle(convertToEntity(contentCustomerStoriesDetails).getTitle());
		}
		if (contentCustomerStoriesDetails.getLongStories() != null) {
			contentCustomerStories.setLongStories(convertToEntity(contentCustomerStoriesDetails).getLongStories());
		}
		if (contentCustomerStoriesDetails.getImagePath() != null) {
			contentCustomerStories.setImagePath(convertToEntity(contentCustomerStoriesDetails).getImagePath());
		}
		if (contentCustomerStoriesDetails.getDescription() != null) {
			contentCustomerStories.setDescription(convertToEntity(contentCustomerStoriesDetails).getDescription());
		}
		if (contentCustomerStoriesDetails.getIsDeleted() != null) {
			contentCustomerStories.setIsDeleted(convertToEntity(contentCustomerStoriesDetails).getIsDeleted());
		}
		if (contentCustomerStoriesDetails.getCreatedBy() != null) {
			contentCustomerStories.setCreatedBy(convertToEntity(contentCustomerStoriesDetails).getCreatedBy());
		}
		if (contentCustomerStoriesDetails.getCreatedOn() != null) {
			contentCustomerStories.setCreatedOn(convertToEntity(contentCustomerStoriesDetails).getCreatedOn());
		}
		if (contentCustomerStoriesDetails.getLastModifiedBy() != null) {
			contentCustomerStories.setLastModifiedBy(convertToEntity(contentCustomerStoriesDetails).getLastModifiedBy());
		}
		if (contentCustomerStoriesDetails.getLastModifiedOn() != null) {
			contentCustomerStories.setLastModifiedOn(convertToEntity(contentCustomerStoriesDetails).getLastModifiedOn());
		}
		ContentCustomerStories updateContentCustomerStories = contentCustomerStoriesRepository.save(contentCustomerStories);

		List<ContentCustomerStories> resultList = new ArrayList<ContentCustomerStories>();
		resultList.add(updateContentCustomerStories);

		if (resultList.isEmpty()) {
			message = "Update Failed!";
		} else {
			message = "Update Success!";
		}

		showHashMap.put("message", message);
		showHashMap.put("total", resultList.size());
		showHashMap.put("data", resultList);

		return showHashMap;
	}

	// Delete a ContentCustomerStories
	@DeleteMapping("/contentCustomerStories/delete/{id}")
	public HashMap<String, Object> delete(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> showHashMap = new HashMap<String, Object>();
		ContentCustomerStories contentCustomerStories = contentCustomerStoriesRepository.findById(id).orElse(null);

		ContentCustomerStoriesDTO contentCustomerStoriesDTO = convertToDTO(contentCustomerStories);
		contentCustomerStoriesRepository.delete(contentCustomerStories);

		showHashMap.put("messages", "Delete data Success!");
		showHashMap.put("deleteData :", contentCustomerStoriesDTO);
		return showHashMap;
	}
}
