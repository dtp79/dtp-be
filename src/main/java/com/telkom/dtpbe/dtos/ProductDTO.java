package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductDTO {
	private long productId;
	private BrandDTO brand;
	private IndustriesDTO industries;
	private ProductTypeDTO productType;
	private String productName;
	private String otherInformation;
	private String imagePath;
	private String duration;
	private Double subscription;
	private String description;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
}
