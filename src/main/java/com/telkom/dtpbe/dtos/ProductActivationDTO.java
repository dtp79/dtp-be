package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductActivationDTO {
	private long productActivationId;
	private ProductDTO product;
	private Date validFrom;
	private Date validUntil;
	private String description;
	private int status;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
}
