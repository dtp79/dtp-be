package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UsersDTO {
	private long userId;
	private CompanyDTO company;
	private RoleDTO role;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String phoneNumber;
	private String imagePath;
	private String tokens;
	private Integer status;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
}
