package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ContactDTO {
	private long contactId;
	private ContactGroupDTO contactGroup;
	private String contactName;
	private String phoneNumber;
	private String email;
	private String otherInformation;
	private String description;
	private Integer status;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;

}
