package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;
import java.util.Date;

import com.telkom.dtpbe.models.Product;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrdersDTO {
	private long orderId;
	private ProductDTO product;
	private Double totalPrice;
	private Date orderDate;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
}
