package com.telkom.dtpbe.dtos;
import java.sql.Timestamp;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ContentFooterDTO implements java.io.Serializable {

	private Long contentFooterId;
	private String title;
	private String description;
	private Short isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;

	
}
