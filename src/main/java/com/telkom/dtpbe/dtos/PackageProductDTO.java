package com.telkom.dtpbe.dtos;

import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PackageProductDTO {
	private long packageProductId;
	private ProductDTO product;
	private String packageProductName;
	private String description;
	private Double subcription;
	private Boolean isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;
}
