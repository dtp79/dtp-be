package com.telkom.dtpbe.dtos;
import java.sql.Timestamp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ContentHeaderDTO implements java.io.Serializable {

	private Long contentHeaderId;
	private String title;
	private String url;
	private Short isDeleted;
	private Long createdBy;
	private Timestamp createdOn;
	private Long lastModifiedBy;
	private Timestamp lastModifiedOn;

	
}
